require 'test_helper'

class UsersControllerTest < ActionController::TestCase

  def setup
    @users = User.count
    @user = User.create(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", 
    password_confirmation: "foobar")
    session[:user_id] = @user.id
  end

  test "should get show" do 
    get :show, id: session[:user_id]
    assert_response :success
    assert_not_nil assigns(:user)
  end 

  test "should get new" do
    get :new 
    assert_response :success
    assert_select "h1", "Users#new"
    assert_select "p", "Find me in app/views/users/new.html.erb"
  end 
  
  test "should create user" do 
    assert_difference('User.count') do
       post :create, user: { first_name: 'Ade', last_name: "Labinjo", email: "adelabinjo@gmail.com", 
       password: "foobar", password_confirmation: "foobar"}
    end 
    
    assert_redirected_to users_path 
    assert_equal "You have successfully registered", flash[:success]
  end 
  
  test "shoud destroy user" do 
    assert_difference "User.count", -1 do 
      delete :destroy, id: @user 
    end 
    
    assert_redirected_to users_path
    assert_equal "User has been deleted", flash[:success]
  end 
  
end
