require 'test_helper'

class SessionsControllerTest < ActionController::TestCase
  def setup
    @user = User.create(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", 
      password_confirmation: "foobar")
    session[:user_id] = @user.id 
  end 

  test "should get new" do 
    get :new 
    assert_response :success
  end
  
  test "should create new session" do 
    post :create, session[:user_id] = @user.id 
  end 
  
  test "should destroy session" do 
    delete :destroy, session[:user_id] = nil 
    assert_redirected_to sessions_path 
    assert_equal "You have successfully logged out", flash.now["success"]
  end 
end
