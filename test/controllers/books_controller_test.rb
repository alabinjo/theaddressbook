require 'test_helper'

class BooksControllerTest < ActionController::TestCase
  
  def setup
    @user = User.create(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", 
      password_confirmation: "foobar")
    session[:user_id] = @user.id
    @book = @user.books.create( name: "Soccer Friends")
  end 
  
    
  test "should get show" do 
    get :show, user_id: session[:user_id], id: @book.id 
  end 

  test "should get new" do
    get :new, user_id: session[:user_id], id: @book.id  
    assert_response :success
  end
  
  test "should create new book" do 
    assert_difference "@user.books.count" do 
      post :create, @user.books.create(name: "Soccer Friends")
      assert_equal("Address book '#{@book.name}' has been created successfully", flash[:success])
    end 
  end 
  
  test "should destroy book" do 
    assert_difference "@user.books.count", -1 do 
      delete :destroy, user_id: @user.id, id: @book.id 
    end
  end 
  
  

end
