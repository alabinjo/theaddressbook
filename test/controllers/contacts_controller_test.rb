require 'test_helper'

class ContactsControllerTest < ActionController::TestCase
  
  def setup
    @user = User.create(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", 
      password_confirmation: "foobar")
    session[:user_id] = @user.id
    @book = @user.books.create( name: "Soccer Friends")
    @contact = @book.contacts.create(first_name: "Ade", last_name: "labinjo", address: "135 marleee") 
  end 
  
  test "should get new" do
    get :new, user_id: session[:user_id], book_id: @book.id
    assert_response :success
  end
  
  test "should create new contact" do 
    assert_difference "@book.contacts.count" do 
      post :create, @book.contacts.create(first_name: "Ade", last_name: "labinjo", address: "135 marleee") 
    end 
    assert_redirected_to user_book_path(@user.id, @book.id)
    assert_equal("A new contact has been successfully created", flash[:success])
  end 
  
  test "should destroy contact" do 
    assert_difference "@book.contacts.count", -1 do 
      delete :destroy, user_id: session[:user_id], book_id: @book.id, id: @contact.id
    end
      assert_redirected_to user_book_path(@user, @book)
      assert_equal("Contact successfully removed", flash[:success] )
  end 

end
