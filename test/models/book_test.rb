require 'test_helper'

class BookTest < ActiveSupport::TestCase
  
  def setup 
    @book = Book.create(user_id: 1, name: "Soccer Friends")
  end 
  
  test "book should be valid" do
    assert @book.valid?
  end 
  
  test "name should be present" do
    @book.name = "   "
    assert_not @book.valid?
  end 
 
  test "name should be at least 3 characters long" do
    @book.name = "a" * 2 
    assert_not @book.valid?
  end 
  
  test "name should not be longer than 155 characters" do
    @book.name = "a" * 156 
    assert_not @book.valid? 
  end 
  
  test "name user_id should be present" do
    @book.user_id = nil
    assert_not @book.valid?
  end 
  
end
