require 'test_helper'

class ContactTest < ActiveSupport::TestCase
  
  def setup
    @user = User.create(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", password_confirmation: "foobar")
    @book = @user.books.create( name: "Soccer Friends")
    @contact = @book.contacts.create(first_name: "Ade", last_name: "labinjo", address: "135 marleee") 
  end 
  
  test "contact should be valid" do 
    @contact.valid?
  end 
  
  test "book id should be present" do 
    @contact.book_id = nil
    assert_not @contact.valid?
  end 
  
  test "first name should be present" do 
    @contact.first_name = "   "
    assert_not @contact.valid?
  end 
  
  test "firstname should not be less than 2 characters" do 
    @contact.first_name = "a" * 1
    assert_not @contact.valid? 
  end 
  
  test "first name should not be longer than 155 characters" do 
    @contact.first_name = "a" * 156 
    assert_not @contact.valid?
  end 
  
  test "last name should be present" do 
    @contact.last_name = "   "
    assert_not @contact.valid?
  end 
  
  test "last name should not be less than 2 characters" do 
    @contact.last_name = "a" * 1
    assert_not @contact.valid? 
  end 
  
  test "last name should not be longer than 155 characters" do 
    @contact.last_name = "a" * 156 
    assert_not @contact.valid?
  end 
  
  test "address should be present" do 
    @contact.address = "   "
    assert_not @contact.valid?
  end 
  
  test "address should not be less than 2 characters" do 
    @contact.address = "a" * 1
    assert_not @contact.valid? 
  end 
  
  test "address should not be longer than 155 characters" do 
    @contact.address = "a" * 156 
    assert_not @contact.valid?
  end 
    
end
