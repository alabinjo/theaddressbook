require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  #This gets run before every test 
  def setup
    @user = User.new(first_name: "John", last_name: "Doe", email: "john@gmail.com", password: "foobar", 
    password_confirmation: "foobar")
  end 
  
  #sanity check to see if we can get a valid user 
  test "should be valid" do
    assert @user.valid?
  end 
  
  test "first_name must be present" do 
    @user.first_name = "  "
    assert_not @user.valid?
  end 
  
  test "email must be present" do 
    @user.email = "    "
    assert_not @user.valid?
  end 
  
  test "first name should be more than 50 character" do 
    @user.first_name = "a" * 51
    assert_not @user.valid? 
  end 
  
  test "email should be more than 255 characters" do 
    @user.email = "a" * 256
    assert_not @user.valid?
  end 
  
  test "email validation should accept valid addresses" do 
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org first.last@foo.jp alice+bob@baz.cn ]
    
    valid_addresses.each do |valid_address|
      @user.email = valid_address  
      assert @user.valid?, "Address #{valid_address.inspect} should be valid"
    end 
  end 
  
  test "email validation should reject invalid addresses" do 
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example. foo@bar_baz.com foo@bar+baz.com ]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "Address #{invalid_address.inspect} should be invalid"
    end 
  end 
  
  test "email address should be unique" do 
    #makes a copy of the user, exist only memory 
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase 
    #save first email 
    @user.save 
    #should not be able to create another user object with that email 
    assert_not duplicate_user.valid?
    
  end 
  
  test "password must be present" do 
    @user.password = "     "
    assert_not @user.valid?
  end 
  
  test "password should be at least 6 characters long" do 
    @user.password = "a" * 5
    @user.password_confirmation = "a" * 5
    assert_not @user.valid?
    
  end 
  
end
