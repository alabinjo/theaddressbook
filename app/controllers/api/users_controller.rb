class Api::UsersController< Api::ApiController  
  
  def show 
    @user = User.find(params[:id])
    render json: @user 
  end 
  
  def create 
    @user = User.new(user_params)
    if @user.save 
      render status: 200, json: { message: "You have successfully registered", user: @user }.to_json 
    else 
      render status: 422, json: { message: "There is an error with your form", user: @user }.to_json
    end
  end 
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    render status: 200, json: { message: "User has been deleted" }.to_json
  end 
  
  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_digest) 
    end 
  
  
end 