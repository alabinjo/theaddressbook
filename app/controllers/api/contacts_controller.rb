class Api::ContactsController < Api::ApiController 
  
  def create
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:book_id])
    @contact = @book.contacts.new(contact_params)
    if @contact.save
      render status: 200, json: {
        message: "A new contact has been successfully created", 
        user: @user, 
        book: @book, 
        contact: @contact 
      }.to_json
    else
      render status: 422, json: { 
        message: "An error occurred", 
        user: @user, 
        book: @book, 
        contact: @contact 
      }.to_json
    end
  end
  
  def destroy 
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:book_id])
    @contact = @book.contacts.find(params[:id])
    flash[:success] = 
    @contact.destroy 
    render status: 200, json: {
      message: "Contact successfully removed",  
      user: @user, 
      book: @book, 
      contact: @contact 
    }.to_json
  end 
  
  private 
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :address, :book_id)
    end 
end