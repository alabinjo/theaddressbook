class Api::BooksController < Api::ApiController 
  
  def show 
    @user = User.find(current_user)
    @book = @user.books.find(params[:id])
    render json: { 
      user: @user, 
      book: @book 
    }.json 
  end 
  
  def create
    @user = User.find(current_user)
    @book = @user.books.build(book_params)
    if @book.save 
      render status: 200, json: { 
        message: "Address book '#{book.name}'' has been created successfully", 
        user: @user, 
        book: @book 
      }.to_json
    else 
      render status: 422, json: { 
        message: "Error creating the book", 
        user: @user, 
        book: @book 
      }.to_json
    end
  end 
  
  def destroy
    @user = User.find(current_user)
    @book = @user.books.find(params[:id])
    @book.destroy 
    render status: 200, json: { 
      message: "Address book succesfully destroyed", 
      user: @user, 
      book: @book 
    }.to_json
  end 
  
  private 
    def book_params 
      params.require(:book).permit(:name)
    end 
  
end 