class ContactsController < ApplicationController
  before_action :require_user
  
  def new
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:book_id])
    @contact = @book.contacts.build
  end
  
  def create
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:book_id])
    @contact = @book.contacts.new(contact_params)
    if @contact.save 
      flash[:success] = "A new contact has been successfully created"
      redirect_to user_book_path(@user.id, @book.id)
    else
      flash.now[:error] = "An error occurred"
      render :new 
    end
  end
  
  def destroy 
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:book_id])
    @contact = @book.contacts.find(params[:id])
    flash[:success] = "Contact successfully removed"
    @contact.destroy 
    redirect_to user_book_path(@user, @book)
  end 
  
  private 
    def contact_params
      params.require(:contact).permit(:first_name, :last_name, :address, :book_id)
    end 
end
