class BooksController < ApplicationController
  before_action :require_user

  
  def show 
    @user = User.find(session[:user_id])
    @book = @user.books.find(params[:id])
  end 

  def new
    @user = User.find(session[:user_id])
    @book = Book.new 
  end
  
  
  def create
    @user = User.find(session[:user_id])
    @book = @user.books.build(book_params)
    if @book.save 
      flash[:success] = "Address book '#{@book.name}'' has been created successfully"
      redirect_to user_path(@user)
    else 
      flash.now[:error] = "Error creating the book"
      render :new 
    end
  end 
  
  def destroy
    @user = User.find(params[:user_id])
    @book = @user.books.find(params[:id])
    flash[:success] = "Address book succesfully destroyed"
    @book.destroy 
    redirect_to user_path(@user)
  end 
  
  private 
    def book_params 
      params.require(:book).permit(:name)
    end 
end
