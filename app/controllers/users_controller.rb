class UsersController < ApplicationController
  #before_action :require_user, except: [:new, :cr

 def show 
    @user = User.find(session[:user_id])
 end 
  
  def new
    @user = User.new 
  end
  
  def create 
    @user = User.new(user_params)
    if @user.save 
      flash[:success] = "You have successfully registered"
      session[:user_id] = @user.id
      redirect_to user_path(session[:user_id]) 
    else 
      flash[:error] = "There is an error with your form"
      render :new 
    end
  end 
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy 
    flash[:success] = "User has been deleted"
    redirect_to users_path
  end 
  
  private
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_digest) 
    end 
  
end
