class SessionsController < ApplicationController
  
  def new
    
  end 
  
  def create
    #note - authentication error was due to form_for vs. for_tag on create form
    #study the difference 
    
    user = User.find_by(email: params[:email] )
    if user.authenticate(params[:password])
      session[:user_id] = user.id 
      flash["success"] = "You are logged in as #{user.first_name}"
      redirect_to user_path(session[:user_id]) 
    else 
      flash.now[:error] = "Please try again"
      render :new
    end
  end 
  
  def destroy
    session[:user_id] = nil 
    flash.now["success"] = "You have successfully logged out"
    redirect_to root_path 
  end 
  
  private 
    def session_params
      params.require(:session).permit(:email, :password)
    end
  
end
