class Book < ActiveRecord::Base
  belongs_to :user
  has_many :contacts 
  
  validates :name, presence: true, length: { within: 3..155 }
  validates :user_id, presence: true 
end
