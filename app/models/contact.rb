class Contact < ActiveRecord::Base
  belongs_to :book
  validates :first_name, presence: true, length: { within: 2..155 }
  validates :last_name,  presence: true, length: { within: 2..155 }
  validates :address,    presence: true, length: { within: 2..155 }
  validates :book_id,    presence: true 
end
