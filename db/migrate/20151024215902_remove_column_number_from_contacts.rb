class RemoveColumnNumberFromContacts < ActiveRecord::Migration
  def change
    remove_column :contacts, :number, :integer 
  end
end
