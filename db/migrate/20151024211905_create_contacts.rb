class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :book_id 
      t.string :first_name 
      t.string :last_name 
      t.integer :number 
      t.string :address 
      t.timestamps null: false
    end
  end
end

